//引入依赖
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
//引入页面组件
import Home from '@/views/Home/index.vue'
import ErrorPage from '@/views/Error/index.vue'
import Login from '@/views/Login/index.vue'
import appStore from '@/store/appStore'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/:pathMatch(.*)*',
    name: 'ErrorPage',
    component: ErrorPage
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      keepAlive: true
    },
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    meta: {
      keepAlive: true
    },
    component: Home
  },
  {
    path: '/setting',
    name: 'Setting',
    meta: {
      keepAlive: false
    },
    component: () => import('@/views/Setting/index.vue')
  },
  {
    path: '/search',
    name: 'Search',
    meta: {
      keepAlive: false
    },
    component: () => import('@/views/Search/index.vue')
  }
]
const router = createRouter({
  //在此处配置路由模式
  history: createWebHistory(),
  routes
})

router.beforeEach(async (to, from) => {
  const store = await appStore()
  if(!store.token && to.name !== 'Login')
    return { name: 'Login' } 

  if (store.token && to.name === 'Login')
    return { name: 'Home' }
})

export default router