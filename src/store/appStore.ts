import { defineStore } from 'pinia'
export default defineStore('appStore', {
  state: () => ({
    token: localStorage.getItem('token') || '',
    userinfo: JSON.parse(localStorage.getItem('userinfo') || '{}')
  })
})