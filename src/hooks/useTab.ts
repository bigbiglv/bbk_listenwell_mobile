/*
* 返回出手指在输入元素的滑动方向的数值
*/
import { clearTimeout } from 'timers';
import { ref, Ref, onMounted } from 'vue';
export default function useTab(el: Ref<HTMLElement | null>, opstion: { tab?: Function, dbTab?: Function, LTab?: Function }) {

  //touch的状态
  const isTouch = ref(false);
  //按住时间
  let TouchTime = 0
  let TouchTimer : NodeJS.Timer
  // let ClearTimer : NodeJS.Timer
  // let TabNum = 1
  // function clearTime(){
  //   console.log('ClearTimer', ClearTimer)
  //   //已有延时器不继续新建
  //   if (ClearTimer) return
  //   ClearTimer = setTimeout(() => {
  //     //一定时间后清除 按下时间TouchTime 和 点击次数 TabNum
  //     TouchTime = 0
  //     TabNum = 1
  //     ClearTimer = null
  //     console.log('清除了',TouchTime)
  //   }, 300);
  // }

  //点击事件结束
  function touchend(){
    TouchTime = 0
    clearInterval(TouchTimer)
    console.log('touchend')
  }
  onMounted(()=>{

    el.value?.addEventListener('touchstart', (e: TouchEvent) => {
      // if(TouchTime){
      //   //当前有按下时间 表示这是一次双击事件的第二次点击
      //   //清除上一次点击生成的延时器 防止数据清空
      //   window.clearTimeout(ClearTimer)
      //   ClearTimer = null
      //   //添加点击数量
      //   TabNum++
      //   console.log('双击', TouchTime, ClearTimer)
      // }

      //开始计算按下时间
      TouchTimer = setInterval(()=>{
        TouchTime+=10
        console.log(TouchTime)
      },10)
    })

    el.value?.addEventListener('touchmove', (e) => {

    })
    el.value?.addEventListener('touchend', (e:Event) => {
      //松开的时候停止计算按下时间
      clearInterval(TouchTimer)
      //根据按下时间分辨单击还是长按
      if (TouchTime < 100) {
        opstion.tab!(e)
      }
      if (TouchTime > 200){
        opstion.LTab!(e)
      }
      touchend()
      //打开清空时间延时器
      // clearTime()
    })
  })

}