import { Bbk as axios } from '@/utils/axios'
namespace Login {
  export interface IParams {
    username:string
    password: string
  }
  // 登录成功后返回的token
  export interface IResData {
    userInfo:{
      avatar:string
      email: string
      id: number
      nickname: string
      password: string
      phone: string
      username: string
    }
    token: string
  }
}
// 用户登录
export const Login = (params: Login.IParams) => {
  // 返回的数据格式可以和服务端约定
  return axios.post<Login.IResData>('/user/login', params);
}