import { Netease as axios } from '@/utils/axios'
import type { ResultData } from '@/utils/axios'
/* 首页轮播图 */
export namespace Banner{
  type Ttype = 'pc' | 'iphone' | 'ipad' | 'android'
  export interface IParams {
    type?: Ttype 
  }
  export interface IBanners{
    imageUrl: string
    song: null
    targetId: number
    targetType: number
    titleColor: string
    typeTitle: string
    url: null | string
    video: null | string
  }
  export interface IResData extends ResultData{
    banners?: IBanners[]
  }
}
export const Banner = (params: Banner.IParams): Promise<Banner.IResData> => {
  return axios.get('/banner', params);
}