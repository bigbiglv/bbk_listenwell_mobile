import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { join } from "path"

export default defineConfig({
  plugins: [vue()],
  server: {
    // open: true,
    // host: '0.0.0.0',	// 本机的局域网IP
    // port: 8080,  // 端口号，一般情况下为8080
    proxy: {
      "/netease": {
        target: "https://netease-cloud-music-api-black-five.vercel.app",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/netease/, "")
      },
      "/bbk": {
        // target: "https://bbk-music-serve.vercel.app",
        target: "http://localhost:5001",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/bbk/, "")
      }
    }
  },
  resolve: {
    alias: {
      '@': join(__dirname, "src"),
    }
  }
})
